// TODO: Bài tập 5 (10/06/2021)
// TODO: Refactor đoạn code sau bằng cách sử dụng async await để tránh hiện tượng callback hell
function hellFunc(callback) {
  firstAsyncCall(function (err, resultA) {
    if (err) {
      callback(err);
      return;
    }
    secondAsyncCallUsing(resultA, function (err, resultB) {
      if (err) {
        callback(err);
        return;
      }
      thirdAsyncCallUsing(resultB, function (err, resultC) {
        if (err) {
          callback(err);
        } else {
          callback(null, doSomethingWith(resultC));
        }
      });
    });
  });
}

// !
function hellFunc2(callback) {
  firstAsyncCall((err) => {
    if (err) return;
    secondAsyncCall((err) => {
      if (err) return;
      thirdAsyncCall((err) => {
        if (err) return;
        console.log("Done");
      });
    });
  });
}

var firstAsyncCall = (err, result) => {
  try {
    setTimeout(() => {
      var result = 100;
      console.log(result);
      return 100;
    }, 1000);
  } catch (error) {
    return (err = error);
  }
};

var secondAsyncCall = (number) => {
  setTimeout(() => {
    var result = 200;
    console.log(result);
    return result;
  }, 3000);
};

var thirdAsyncCall = (number) => {
  setTimeout(() => {
    var result = 300;
    console.log(result);
    return result;
  }, 2000);
};

// hellFunc2();
// firstAsyncCall();
// secondAsyncCall();
// thirdAsyncCall();
console.log("--------------------------------");
// ((a) => {
//   console.log(
//     setTimeout(() => {
//       setImmediate;
//     }, 1000).refresh()
//   );
//   if (a) {
//     const b = setTimeout(() => {
//       return 2;
//     }, 1000);
//     if (b === 2) {
//       const c = setTimeout(() => b + 1, 2000);
//       if (c === 3) {
//         console.log(c);
//       } else console.log("if 3 break!");
//     } else console.log("if 2 break!");
//   } else console.log("if 1 break!");
// })(1);

[1, 2, 3, 4].map((val, index) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(val), index * 1000);
    setImmediate()
  }).then((num) => console.log(num));
});
