const { fn_1, fn_2, fn_3, fn_4, fn_fail } = require("./api");
const { getTiming } = require("./util");
// const numbers = Array(1000000);

// console.time("for");
// for (let a = 0; a < numbers.length; a++) {}
// console.timeEnd("for");

// console.time("for of");
// for (const a of numbers) {
// }
// console.timeEnd("for of");

// console.time("forEach");
// numbers.forEach((e) => {});
// console.timeEnd("forEach");

// console.time("map");
// numbers.map((x) => x);
// console.timeEnd("map");

let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

// console.time("filter");
// numbers.filter((number) => number % 2 === 0);
// console.timeEnd("filter");

// console.time("map");
// numbers = numbers.map((x) => x * 2);
// console.timeEnd("map");

// console.time("reduce");
// numbers = numbers.reduce((sum, v) => Math.max(sum, v), 55);
// console.timeEnd("reduce");

(async () => {
  const list = [];
  const data = [];
  list.push(fn_1, fn_2, fn_3, fn_4);
//   list.push(fn_1, fn_2, fn_fail, fn_3, fn_4);

  // TODO: for_of: 2.050s
  // console.time('for_of')
  // for (const i of list) {
  //   console.log(await i());
  // }
  // console.timeEnd('for_of')

  // TODO: forEach: 1.424ms
//   console.time("forEach");
//   list.forEach(async (api) => {
//     console.log(await api());
//     const a = await api();
//   });
//   console.timeEnd("forEach");

    console.time('promise.all')
   Promise.all(
     list.map(async (el) => {
       console.log(await el());
     })
   );
   console.timeEnd('promise.all')

})();
