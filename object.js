// * từ khóa this đại diện cho hàm đang chứa nó
// * được sử dụng để truy cập được các thuộc tính và phương thức của đối tượng

// Todo: cách khai báo 1 (ES 5)
function User(name, lastName) {
  this.name = name;
  this.lastName = lastName;
  this.getName = () => {
    this.lastName;
    return "hello " + this.lastName;
  };
}

// Todo: cách khai báo 2 (ES 5)
const Person = function (name) {
  this.name = name;
};

// Todo: cách khai báo 3 (ES 6)
class Person1 {
  constructor(name) {
    this.name = name;
  }
}

const user = new User("khoa", "vo");

const person = new Person("quang");

const person1 = new Person1("huy");
console.log(person1);
