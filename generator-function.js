const fetch = require("fetch").FetchStream;

function getUser(name) {
  return new Promise((res) => {
    setTimeout((x) => res("khoa"), 1000);
  });
}

function* getName(name) {
  const userName = yield getUser(name);
  console.log(userName);
}

function runGeneratorLikeAsyncAwait(generator) {
  const generatorObject = generator();

  function loopUntilDone(value) {
    const next = generatorObject.next(value);

    if (next.done) return;

    next.value.then((data) => loopUntilDone(data));
  }

  loopUntilDone();
}
runGeneratorLikeAsyncAwait(getName);
