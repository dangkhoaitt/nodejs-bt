const times = [500, 300, 200, 600, 800, 700, 400, 900, 100];

const fn_1 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ status: 200, data: "success" });
    }, Math.random() * 900 + 100);
  });
};

const fn_2 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ status: 200, data: "success" });
    }, Math.random() * 900 + 100);
  });
};

const fn_3 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ status: 200, data: "success" });
    }, Math.random() * 900 + 100);
  });
};

const fn_4 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ status: 200, data: "success" });
    }, Math.random() * 900 + 100);
  });
};

const fn_5 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ status: 200, data: "success" });
    }, Math.random() * 900 + 100);
  });
};

const fn_6 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ status: 200, data: "success" });
    }, Math.random() * 900 + 100);
  });
};

const fn_7 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ status: 200, data: "success" });
    }, Math.random() * 900 + 100);
  });
};

const fn_fail = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      reject({ status: 500, data: "error" });
    }, 1000);
  });
};

module.exports.fn_1 = fn_1;
module.exports.fn_2 = fn_2;
module.exports.fn_3 = fn_3;
module.exports.fn_4 = fn_4;
module.exports.fn_5 = fn_5;
module.exports.fn_fail = fn_fail;
