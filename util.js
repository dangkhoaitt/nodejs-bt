const getTiming = (key, callback) => {
  console.time(key);
  callback();
  console.timeEnd(key);
};

module.exports.getTiming = getTiming;
